1.This repo exist because it is a coding assignment as part of an interview process.

The specific program takes a CSV file, and then processes it. Putting all rows into a database that don't contain errors.
Then the program logs what happened, such as the errors.

2. This program includes 2 external libraries, sqlite-jdbc and opencsv.
They are sourced from maven,
https://search.maven.org/artifact/com.opencsv/opencsv/5.3/jar
https://search.maven.org/artifact/org.xerial/sqlite-jdbc/3.34.0/jar

Compiling this project should require the Main file(The only File), and importing the 2 external libraries

To run, run the main file, and then enter CSV file name into the command prompt, but without the extension.
Example. test.csv -> test

3. My approach was to value simplicity. Most errors are caught and then crash and just printed out to identify the location.
Future version could add more as needed. Because the program seemed small, I put some of the variables into the
static global for easier access.

I put things in functions, if I thought they could be easily separated out, and wouldn't effect the speed much. My functions were
all static because there wasn't a need for any custom classes.

I was also concerned that the initialization of the database connections and the connection to the log files might cause slowdowns.
So I initialized them at the start of the loop and just passed it through.

Changes

I added an extra column ID into the table, so there was a guaranteed primary key.
I added the line number and error type to the error log file, for errors
Max Column Sizes were chosen for each column, (A, B, C ext...), and if a field exceeded this it is treated as an error

Assumptions
Only a completely empty line is an error. All whitespace is considered valid
The previous database and log's are always deleted and replaced if same program is re-run
I limited the image size to 2000 characters
I make no assumptions of the data, and just treat it all as text even though some columns imply specifics, like True False
I assume the format will not change, each column is associated with a specific variable (A, B, C ,D ...).
