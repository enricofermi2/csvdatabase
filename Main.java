import java.sql.*;

import com.opencsv.*;

import java.io.File;
import java.io.*;

public class Main {



    //max size for each column of the csv file, if larger than this throws error
    public static final int[] maxInputSize = {30, 30, 50, 10, 2000, 50, 10, 10, 10, 50};
    public static final int varNum = 10;

    //globals for names
    public static String logName = "";
    public static String badLogName = "";
    public static String dBaseName = "";
    public static String csvName = "";

    public static Connection c = null;


    public static void main(String args[]) {

        try {

            BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please enter CSV file name, without the file extension");
            String in = r.readLine();
            logName = in + ".log";
            badLogName = in + "-bad.log";
            dBaseName = in + ".db";
            csvName = in + ".csv";
            r.close();

        } catch (Exception E) {
            System.out.println("File Can't be selected, exiting program");
            System.exit(1);
        }


        CSVReader reader = null;

        try {
            reader = new CSVReader(new FileReader(csvName));
        } catch (Exception E) {
            System.out.println("CSV File can't be read, exiting");
            System.exit(1);
        }
        try {
            c = DriverManager.getConnection("jdbc:sqlite:" + dBaseName);
        } catch (Exception E) {
            System.out.println("Can't Connect to Database, exiting");
            System.exit(1);
        }
        createDbase();
        makeLogFile();

        System.out.println("Processing data");

        try {
            String[] line = reader.readNext();
            line = reader.readNext();//skip first line

            int count = 1;
            int bacCount = 0;
            //File fName = ;
            BufferedWriter bufWrite = new BufferedWriter(new FileWriter(new File(badLogName)));

            while (line != null) {
                count++;
                Boolean error = false;
                if (line.length == varNum) {
                    for (int i = 0; i < varNum; i++) {
                        if (line[i].equals("")) {
                            addError(bufWrite, 0, count, line);
                            error = true;
                        }
                        if (line[i].length() > maxInputSize[i]) {
                            addError(bufWrite, 1, count, line);
                            error = true;
                        }
                    }
                } else {
                    addError(bufWrite, 2, count, line);
                    error = true;
                }

                if (error) {
                    bacCount++;
                } else {
                    addRow(line, count);
                    //push to database
                }
                line = reader.readNext();
            }
            bufWrite.close();
            makeStatLog(count, bacCount);
            c.close();
        } catch (Exception E) {
            System.out.println("General IO error, System Crash");
            System.exit(1);
        }
        System.out.println("Done");

    }

    //adds a line to the bad-log file, that includes the lines data, the error and where in the CSV file it occurred
    public static void addError(BufferedWriter bw, int error, int lineNum, String[] fields) {
        try {
            if (error == 0) {
                bw.write("Empty Field Error on Line " + lineNum + " \n");
            }
            if (error == 1) {
                bw.write("Field to Big Error on Line " + lineNum + " \n");
            }
            if (error == 3) {
                bw.write("Line has Wrong number of Variables on Line " + lineNum + " \n");
            }
            String totalRow = "";
            for (int i = 0; i < fields.length; i++) {
                totalRow += fields[i] + " || ";
            }
            bw.write(totalRow + "\n");
        } catch (Exception E) {
            System.out.println("Couldn't Write to error Log");
        }
    }

    //inserts a single row into the database
    public static void addRow(String[] row, int lineNum) {
        try {
            String insert = "INSERT INTO PERSON(ID,A,B,C,D,E,F,G,H,I,J) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = c.prepareStatement(insert);
            ps.setString(1, (lineNum + ""));
            for (int i = 0; i < varNum; i++) {
                ps.setString(i + 2, row[i]);
            }
            ps.execute();
            ps.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //This gets the selected row by ID and print it out
    //Mainly for testing purposed to validate insert
    public static void getRow(int ID) {
        String select = "SELECT * FROM PERSON WHERE ID = " + ID;
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(select);
            rs.next();
            System.out.println(rs.getString("A"));
            System.out.println(rs.getString("B"));
            System.out.println(rs.getString("C"));
            System.out.println(rs.getString("D"));
            System.out.println(rs.getString("E"));
            System.out.println(rs.getString("F"));
            System.out.println(rs.getString("G"));
            System.out.println(rs.getString("H"));
            System.out.println(rs.getString("I"));
            System.out.println(rs.getString("J"));
            s.close();
        } catch (Exception e) {
            System.out.println("A row failed to insert");
        }
    }

    //creates the database, and drops the old one if it exist
    public static void createDbase() {

        String drop = "DROP TABLE PERSON";
        String sqlMake = "CREATE TABLE PERSON " +
                "(ID INT NOT NULL PRIMARY KEY," +
                " A  VARCHAR(30)    NOT NULL, " +
                " B  VARCHAR(30)    NOT NULL, " +
                " C  VARCHAR(50)    NOT NULL, " +
                " D  VARCHAR(10)    NOT NULL, " +
                " E  TEXT   NOT NULL, " +
                " F  VARCHAR(50)    NOT NULL, " +
                " G  VARCHAR(10)    NOT NULL, " +
                " H  VARCHAR(10)    NOT NULL, " +
                " I  VARCHAR(10)    NOT NULL, " +
                " J  VARCHAR(50) NOT NULL)";


        try {
            c = DriverManager.getConnection("jdbc:sqlite:" + dBaseName);
            Statement stmt = c.createStatement();
            try {
                stmt.execute(drop);
            } catch (Exception E) {
                //code always drops table, even if doesn't exist, this throws error which is ignored
            }
            stmt.execute(sqlMake);
            stmt.close();
        } catch (Exception e) {
            System.out.println("Database Table Failed to Initialize");
        }
    }


    //makes the log file that shows results
    public static void makeStatLog(int total, int badCount) {
        try {
            File statLog = new File(logName);
            //Always delete old log file, and replace with clean
            statLog.delete();
            statLog.createNewFile();
            BufferedWriter bufWrite = new BufferedWriter(new FileWriter(statLog));
            bufWrite.write("Total Count: " + total + "\n");
            bufWrite.write("Good Rows: " + (total - badCount) + "\n");
            bufWrite.write("Bad Rows: " + badCount + "\n");
            bufWrite.close();
        } catch (Exception E) {
            System.out.println("Stat Log creation Failed");
        }
    }

    //initializes an empty bad-log to guarantee it exists.
    public static void makeLogFile() {
        try {
            File errLog = new File(badLogName);
            //Always delete old log file, and replace with clean
            errLog.delete();
            errLog.createNewFile();
        } catch (Exception E) {
            System.out.println("The bad log file failed to initialize");
        }
    }


}
